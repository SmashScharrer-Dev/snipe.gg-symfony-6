<h1 align="center">Welcome to snipe-gg-symfony-6 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="https://gitlab.com/SmashScharrer-Dev/snipe.gg-symfony-6/-/blob/main/LICENSE.md" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> A League of Legends statistics application to snipe the best EUW Top laner : Kevin. This app is developped with Symfony framework.

## Install

```sh
npm install
```

## Author

👤 **GUIRADO-PATRICO Nathan**


## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitlab.com/SmashScharrer-Dev/demyqewiki-app-symfony-6.1/-/issues). You can also take a look at the [contributing guide](https://gitlab.com/SmashScharrer-Dev/snipe.gg-symfony-6/-/blob/main/CONTRIBUTING.md).

## Show your support

Give a ⭐️ if this project helped you!

## 📝 License

This project is [MIT](https://gitlab.com/SmashScharrer-Dev/snipe.gg-symfony-6/-/blob/main/LICENSE.md) licensed.

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_