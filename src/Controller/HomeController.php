<?php

namespace App\Controller;

use App\Service\CacheService;
use Psr\Cache\InvalidArgumentException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use RuntimeException;

#[Route('/home')]
class HomeController extends AbstractController
{
    /**
     * @param CacheService $cacheService
     */
    public function __construct(private readonly CacheService $cacheService)
    {
    }

    /**
     * @throws RuntimeException|InvalidArgumentException
     */
    #[Route('/', name: 'app_home', methods: ['GET'])]
    public function __invoke(): Response
    {
        // ----- Requête : Récupérer dernière version (via cache) -----
        $latestVersion = $this->cacheService->getLatestVersionCache();

        // ----- Requête : Récupérer données du summoner (via cache) -----
        $summoner = $this->cacheService->getSummonerCache();

        return $this->render('home/index.html.twig', [
            'summoner' => $summoner,
            'latestVersion' => $latestVersion,
            'idIcone' => $summoner["profileIconId"]
        ]);
    }
}
