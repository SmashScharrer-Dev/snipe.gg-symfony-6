<?php

namespace App\Service;

use App\Service\DDragonAPIService;
use App\Service\RiotAPIService;
use Psr\Cache\InvalidArgumentException;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;

class CacheService
{
    public function __construct(private readonly CacheInterface $cache, private readonly DDragonAPIService $ddragonAPIService, private readonly RiotAPIService $riotAPIService)
    {
    }

    /**
     * @return array<int, string>
     * @throws InvalidArgumentException
     */
    public function getVersionsCache(): array
    {
        return $this->cache->get('versions', function(ItemInterface $item) {
            $item->expiresAfter(3600);

            return $this->ddragonAPIService->findVersions();
        });
    }

    /**
     * @return string
     * @throws InvalidArgumentException
     */
    public function getLatestVersionCache(): string
    {
        return $this->cache->get('latest_version', function(ItemInterface $item) {
            $item->expiresAfter(3600);

            return $this->ddragonAPIService->findLatestVersion();
        });
    }

    /**
     * @return array<int, string>
     * @throws InvalidArgumentException
     */
    public function getSummonerCache(): array
    {
        return $this->cache->get('summoner', function(ItemInterface $item) {
            $item->expiresAfter(3600);

            return $this->riotAPIService->findSummonerByName();
        });
    }
}
