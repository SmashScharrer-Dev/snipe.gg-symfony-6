<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface as ClientExceptionInterfaceAlias;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RiotAPIService
{
    /**
     * @param HttpClientInterface $client
     * @param string $apiLink
     * @param string $apiKey
     */
    public function __construct(private readonly HttpClientInterface $client, private readonly string $apiLink, private readonly string $apiKey)
    {
    }

    /**
     * @param string $name
     * @return array
     * @throws ClientExceptionInterfaceAlias
     * @throws DecodingExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function findSummonerByName(): array
    {
        $response = $this->client->request(
            'GET',
            $this->apiLink . '/lol/summoner/v4/summoners/by-name/Sn0W38', [
                'query' => [
                    'api_key' => $this->apiKey
                ],
            ]
        );

        return $response->toArray();
    }
}
