# **New feature** : Lorem ipsum

## 1. Context branch
- From : %{source_branch}
- To : %{target_branch}

## 2. What this MR does / why we need it ?
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam dictum tellus nulla. Mauris et ultricies felis. Suspendisse sollicitudin eget arcu.

## 3. Make sure that you've checked the boxes below before you submit MR
- [ ] I have run GitLab pipeline, and there is no error. 
- [ ] no conflict with master branch.
- [ ] I have update the CHANGELOG.md file.

## 4. Update list
- Lorem ipsum dolor sit amet.
- Lorem ipsum dolor sit amet.
- Lorem ipsum dolor sit amet.